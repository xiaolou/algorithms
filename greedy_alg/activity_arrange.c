#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

#define random(x) ((rand())%x)

typedef struct activity_time {
  int start_time;
  int end_time;
} struct_a;

int __sort_by_endtime(struct_a *input, int len) {
  if (input==NULL)
    return 1;

  int i=0;
  int j=0;
  int tmp=0;
  for (i=1; i<len; i++) {
    for (j=i-1; j>=0; j--) {
      if (input[j].end_time > input[j+1].end_time) {
        tmp=input[j+1].end_time;
        input[j+1].end_time=input[j].end_time;
        input[j].end_time=tmp;

        tmp=input[j+1].start_time;
        input[j+1].start_time=input[j].start_time;
        input[j].start_time=tmp;
      }
    }
  }
  return 0;
}

int activity_arrange(struct_a *input_activity, struct_a *output_activity, int input_len) {
  if (input_activity==NULL || output_activity==NULL)
    return 1;

  __sort_by_endtime(input_activity, input_len);

  int last=0;
  int index;
  int out_len=0;
  memcpy(output_activity, input_activity, sizeof(struct_a));
  out_len+=1;
  for (index=1; index<input_len; index++) {
    if(input_activity[index].start_time > input_activity[last].end_time) {
      memcpy(output_activity+out_len, input_activity+index, sizeof(struct_a));
      out_len+=1;
      last=index;
    }
  }

  return 0;
}

int print(struct_a *array, int len) {
  int i;
  for (i=0; i<len; i++) {
    printf("%d, %d\n", array[i].start_time, array[i].end_time);
  }
  printf("\n");
  return 0;
}

int main() {
  int num=11;
  int i=0;
  srand((int)time(0));
  struct_a *input_arr = (struct_a *)malloc(num*sizeof(struct_a));
  struct_a *out_arr = (struct_a *)malloc(num*sizeof(struct_a));
  for (i=0; i<num; i++) {
    int start=random(100);
    input_arr[i].start_time=start;
    input_arr[i].end_time=start+1+random(50);
  }
  print(input_arr, num); 

  activity_arrange(input_arr, out_arr, num);
  print(out_arr, num);

  free(input_arr);
  free(out_arr);
  return 0;
}
